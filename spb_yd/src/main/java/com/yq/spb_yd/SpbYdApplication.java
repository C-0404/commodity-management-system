package com.yq.spb_yd;

import ch.qos.logback.classic.pattern.MessageConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.yq.spb_yd.mapper")
public class SpbYdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpbYdApplication.class, args);
    }

}
