package com.yq.spb_yd.controller;

import com.yq.spb_yd.service.TestService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestController {
    @Resource
    private TestService testService;

    /**
     * 测试控制器
     */
    @RequestMapping("test")
    public String test(){
        return testService.testSelect();
    }
}
