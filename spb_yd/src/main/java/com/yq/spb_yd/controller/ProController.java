package com.yq.spb_yd.controller;

import com.yq.spb_yd.domain.ProEntity;
import com.yq.spb_yd.service.ProService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ProController {
    //注入Service
    @Resource
    private ProService proService;

    /**
     * 查询所有商品
     */
    @RequestMapping("getPros")
    public List<ProEntity> getPros(){
        return proService.getPros();
    }

    /**
     * 添加商品
     */
    @RequestMapping("addPro")
    public String addPro(ProEntity pro){
        if(proService.addPro(pro)){
            return "success";
        }else {
            return "failed";
        }
    }

    /**
     * 修改商品
     */
    @RequestMapping("modifyPro")
    public String modifyPro(ProEntity pro){
        return proService.modifyPro(pro) ? "success" : "failed";
    }

    /**
     * 删除商品
     */
    @RequestMapping("removePro")
    public String removePro(int id){
        return proService.removePro(id) ? "success" : "failed";
    }
}
