package com.yq.spb_yd.controller;

import com.yq.spb_yd.domain.ProTypeEntity;
import com.yq.spb_yd.service.ProTypeService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ProTypeController {
    //注入Service
    @Resource
    private ProTypeService proTypeService;

    /**
     * 请求查询所有商品类型
     */
    @RequestMapping("getProTypes")
    public List<ProTypeEntity> getProTypes(){
        return proTypeService.getProTypes();
    }

    /**
     * 添加类型
     */
    @RequestMapping("addProType")
    public String addProType(ProTypeEntity pt){
        if(proTypeService.addProType(pt)){
            //添加成功
            return "success";
        }else{
            //添加失败
            return "failed";
        }
    }

    /**
     * 删除类型
     */
    @RequestMapping("removeProType")
    public String removeProType(int id){
        if(proTypeService.removeProType(id)){
            return "success";
        }{
            return "failed";
        }

    }

    /**
     * 修改类型
     */
    @RequestMapping("modProType")
    public String modProType(ProTypeEntity pt){
        if(proTypeService.modifyProType(pt)){
            return "success";
        }{
            return "failed";
        }

    }
}
