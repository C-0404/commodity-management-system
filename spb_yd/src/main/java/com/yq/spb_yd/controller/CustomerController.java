package com.yq.spb_yd.controller;

import com.yq.spb_yd.domain.CustomerEntity;
import com.yq.spb_yd.service.CustomerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class CustomerController {
    //注入Service
    @Resource
    private CustomerService cusService;

    /**
     * 查询所有商品
     */
    @RequestMapping("getCustomers")
    public List<CustomerEntity> getCustomers(){
        return cusService.getCustomers();
    }

    /**
     * 添加商品
     */
    @RequestMapping("addCustomer")
    public String addCustomer(CustomerEntity Customer){
        if(cusService.addCustomer(Customer)){
            return "success";
        }else {
            return "failed";
        }
    }

    /**
     * 修改商品
     */
    @RequestMapping("modifyCustomer")
    public String modifyCustomer(CustomerEntity Customer){
        return cusService.modifyCustomer(Customer) ? "success" : "failed";
    }

    /**
     * 删除商品
     */
    @RequestMapping("removeCustomer")
    public String removeCustomer(int id){
        return cusService.removeCustomer(id) ? "success" : "failed";
    }
}
