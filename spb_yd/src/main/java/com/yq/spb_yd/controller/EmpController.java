package com.yq.spb_yd.controller;

import com.yq.spb_yd.domain.EmpEntity;
import com.yq.spb_yd.service.EmpService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

//添加RestController注解,使其可以提供 Ajax的JSON服务
@RestController
public class EmpController {
    //注入Service
    @Resource
    private EmpService empService;

    /**
     * 登录请求
     * 配置请求路径：RequestMapping 注解配置请求路径
     */
    @RequestMapping("/login")
    public EmpEntity login(String admName, String admPwd){
        //使用Service对象进行登录查询
       EmpEntity logEmp = empService.login(admName, admPwd);
       //如果没有查询到，这封装一个对象
        if(logEmp == null){
            logEmp = new EmpEntity();
            logEmp.setEmpName("log field");
        }
       return logEmp;
    }

    /**
     * 修改个人信息请求
     */
    @RequestMapping("modifyEmp")
    public String modifyEmp(EmpEntity emp){
        return empService.modEmp(emp) ? "success" : "failed";
    }

    /**
     * 修改个人密码
     */
    @RequestMapping("modPwd")
    public String modPwd(String empNo, String oldPwd, String newPwd){
        return empService.modPwd(empNo, oldPwd, newPwd) ? "success" : "failed";
    }

}
