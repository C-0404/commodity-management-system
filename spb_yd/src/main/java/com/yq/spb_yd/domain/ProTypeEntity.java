package com.yq.spb_yd.domain;

public class ProTypeEntity {
    private int ptId;
    private String ptName;
    private String ptDesc;

    public int getPtId() {
        return ptId;
    }

    public void setPtId(int ptId) {
        this.ptId = ptId;
    }

    public String getPtName() {
        return ptName;
    }

    public void setPtName(String ptName) {
        this.ptName = ptName;
    }

    public String getPtDesc() {
        return ptDesc;
    }

    public void setPtDesc(String ptDesc) {
        this.ptDesc = ptDesc;
    }
}
