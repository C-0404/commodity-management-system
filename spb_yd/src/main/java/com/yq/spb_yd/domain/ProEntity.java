package com.yq.spb_yd.domain;

public class ProEntity {
    private int proId;
    private String proName;
    private float proPrice;
    private ProTypeEntity proType;

    public int getProId() {
        return proId;
    }

    public void setProId(int proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public float getProPrice() {
        return proPrice;
    }

    public void setProPrice(float proPrice) {
        this.proPrice = proPrice;
    }

    public ProTypeEntity getProType() {
        return proType;
    }

    public void setProType(ProTypeEntity proType) {
        this.proType = proType;
    }
}
