package com.yq.spb_yd.domain;

/**
 * 类名命名规范  表名+Entity， 大驼峰规范
 */
public class EmpEntity {
    //使用小驼峰命名规范定义属性和列名相同（去掉下划线）
    private int empNo;
    private String empName;
    private String empPwd;

    // 使用快捷键： Alt + insert 弹出快捷菜单，选择 Getter And Setter 项后，
    // 选择所有属性并生成其getXXX和setXXX方法
    public int getEmpNo() {
        return empNo;
    }

    public void setEmpNo(int empNo) {
        this.empNo = empNo;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpPwd() {
        return empPwd;
    }

    public void setEmpPwd(String empPwd) {
        this.empPwd = empPwd;
    }
}
