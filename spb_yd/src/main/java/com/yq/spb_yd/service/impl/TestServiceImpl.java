package com.yq.spb_yd.service.impl;

import com.yq.spb_yd.mapper.TestMapper;
import com.yq.spb_yd.service.TestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TestServiceImpl implements TestService {
    @Resource
    private TestMapper testMapper;

    @Override
    public String testSelect() {
        return testMapper.testSelect();
    }
}
