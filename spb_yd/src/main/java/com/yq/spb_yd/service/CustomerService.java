package com.yq.spb_yd.service;

import com.yq.spb_yd.domain.CustomerEntity;

import java.util.List;

public interface CustomerService {
    /**
     * 查询所有商品
     */
    public List<CustomerEntity> getCustomers();
    /**
     * 修改商品
     */
    public boolean modifyCustomer(CustomerEntity Customer);
    /**
     * 删除商品
     */
    public boolean removeCustomer(int id);
    /**
     * 添加商品
     */
    public boolean addCustomer(CustomerEntity cus);
}
