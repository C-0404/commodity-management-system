package com.yq.spb_yd.service;

import com.yq.spb_yd.domain.ProTypeEntity;

import java.util.List;

public interface ProTypeService {
    /**
     * 查询所有商品
     */
    public List<ProTypeEntity> getProTypes();
    /**
     * 修改商品
     */
    public boolean modifyProType(ProTypeEntity pt);
    /**
     * 删除商品
     */
    public boolean removeProType(int id);
    /**
     * 添加商品
     */
    public boolean addProType(ProTypeEntity pt);
}
