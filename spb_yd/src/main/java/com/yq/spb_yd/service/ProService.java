package com.yq.spb_yd.service;

import com.yq.spb_yd.domain.ProEntity;

import java.util.List;

public interface ProService {
    /**
     * 查询所有商品
     */
    public List<ProEntity> getPros();
    /**
     * 修改商品
     */
    public boolean modifyPro(ProEntity pro);
    /**
     * 删除商品
     */
    public boolean removePro(int id);
    /**
     * 添加商品
     */
    public boolean addPro(ProEntity pt);
}
