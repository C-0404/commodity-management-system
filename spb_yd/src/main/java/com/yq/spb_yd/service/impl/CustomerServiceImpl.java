package com.yq.spb_yd.service.impl;

import com.yq.spb_yd.domain.CustomerEntity;
import com.yq.spb_yd.mapper.CustomerMapper;
import com.yq.spb_yd.service.CustomerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    //注入Mapper
    @Resource
    private CustomerMapper cusMapper;

    @Override
    public List<CustomerEntity> getCustomers() {
        return cusMapper.getCustomers();
    }

    @Override
    public boolean modifyCustomer(CustomerEntity cus) {
        return cusMapper.modifyCustomer(cus) > 0;
    }

    @Override
    public boolean removeCustomer(int id) {
        return cusMapper.removeCustomer(id) > 0;
    }

    @Override
    public boolean addCustomer(CustomerEntity cus) {
        return cusMapper.addCustomer(cus) > 0;
    }
}
