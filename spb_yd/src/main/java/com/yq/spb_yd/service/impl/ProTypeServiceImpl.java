package com.yq.spb_yd.service.impl;

import com.yq.spb_yd.domain.ProTypeEntity;
import com.yq.spb_yd.mapper.ProTypeMapper;
import com.yq.spb_yd.service.ProTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProTypeServiceImpl implements ProTypeService {
    //注入可查询数据库的 Mapper
    @Resource
    private ProTypeMapper proTypeMapper;

    @Override
    public List<ProTypeEntity> getProTypes() {
        return proTypeMapper.getProTypes();
    }

    @Override
    public boolean modifyProType(ProTypeEntity pt) {
        return proTypeMapper.modifyProType(pt) > 0;
    }

    @Override
    public boolean removeProType(int id) {
        return proTypeMapper.removeProType(id) > 0;
    }

    @Override
    public boolean addProType(ProTypeEntity pt) {
        return proTypeMapper.addProType(pt) > 0;
    }
}
