package com.yq.spb_yd.service;

import com.yq.spb_yd.domain.EmpEntity;

import java.util.Map;

public interface EmpService {
    /**
     * 登录
     */
    public EmpEntity login(String logName, String logPwd);

    /**
     * 修改用户信息
     */
    public boolean modEmp(EmpEntity emp);

    /**
     * 修改密码
     * @param params
     * @return
     */
    public boolean modPwd(String empNo, String oldPwd, String newPwd);
}
