package com.yq.spb_yd.service.impl;

import com.yq.spb_yd.domain.EmpEntity;
import com.yq.spb_yd.mapper.EmpMapper;
import com.yq.spb_yd.service.EmpService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

//添加业务逻辑层注解
@Service
public class EmpServiceImpl implements EmpService {
    //使用Resource注解 注入Mapper对象
    @Resource
    private EmpMapper empMapper;

    @Override
    public EmpEntity login(String logName, String logPwd) {
        //将传入的参数封装为Map
        Map<String, Object> params = new HashMap<>();
        //封装登录用户名和密码到 查询参数的map中
        params.put("logName", logName);
        params.put("logPwd", logPwd);
        //访问Mybatis，获得登录用户
        EmpEntity logEmp =  empMapper.login(params);
        return logEmp;
    }

    @Override
    public boolean modEmp(EmpEntity emp) {
        return empMapper.modEmp(emp) > 0;
    }

    @Override
    public boolean modPwd(String empNo, String oldPwd, String newPwd) {
        //封装参数到Map
        Map<String, Object> params = new HashMap<>();
        params.put("empNo", empNo);
        params.put("oldPwd", oldPwd);
        params.put("newPwd", newPwd);
        return empMapper.modPwd(params) > 0;
    }
}
