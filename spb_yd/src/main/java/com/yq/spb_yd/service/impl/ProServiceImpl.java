package com.yq.spb_yd.service.impl;

import com.yq.spb_yd.domain.ProEntity;
import com.yq.spb_yd.mapper.ProMapper;
import com.yq.spb_yd.service.ProService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProServiceImpl implements ProService {
    //注入Mapper
    @Resource
    private ProMapper proMapper;

    @Override
    public List<ProEntity> getPros() {
        return proMapper.getPros();
    }

    @Override
    public boolean modifyPro(ProEntity pro) {
        return proMapper.modifyPro(pro) > 0;
    }

    @Override
    public boolean removePro(int id) {
        return proMapper.removePro(id) > 0;
    }

    @Override
    public boolean addPro(ProEntity pt) {
        return proMapper.addPro(pt) > 0;
    }
}
