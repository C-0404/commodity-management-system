package com.yq.spb_yd.mapper;

import com.yq.spb_yd.domain.ProTypeEntity;

import java.util.List;

public interface ProTypeMapper {
    /**
     * 查询所有商品
     */
    public List<ProTypeEntity> getProTypes();
    /**
     * 修改商品
     */
    public int modifyProType(ProTypeEntity pt);
    /**
     * 删除商品
     */
    public int removeProType(int id);
    /**
     * 添加商品
     */
    public int addProType(ProTypeEntity pt);
}
