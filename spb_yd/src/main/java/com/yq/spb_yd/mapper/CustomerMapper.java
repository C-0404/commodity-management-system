package com.yq.spb_yd.mapper;

import com.yq.spb_yd.domain.CustomerEntity;

import java.util.List;

public interface CustomerMapper {
    /**
     * 查询所有商品
     */
    public List<CustomerEntity> getCustomers();
    /**
     * 修改商品
     */
    public int modifyCustomer(CustomerEntity cus);
    /**
     * 删除商品
     */
    public int removeCustomer(int id);
    /**
     * 添加商品
     */
    public int addCustomer(CustomerEntity cus);
}
