package com.yq.spb_yd.mapper;

import com.yq.spb_yd.domain.ProEntity;
import com.yq.spb_yd.domain.ProTypeEntity;

import java.util.List;

public interface ProMapper {
    /**
     * 查询所有商品
     */
    public List<ProEntity> getPros();
    /**
     * 修改商品
     */
    public int modifyPro(ProEntity pro);
    /**
     * 删除商品
     */
    public int removePro(int id);
    /**
     * 添加商品
     */
    public int addPro(ProEntity pt);
}
