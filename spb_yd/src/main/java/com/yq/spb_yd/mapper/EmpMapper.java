package com.yq.spb_yd.mapper;

import com.yq.spb_yd.domain.EmpEntity;

import java.util.Map;

/**
 * 命名规范： 表明+Mapper 大驼峰规范
 */
public interface EmpMapper {
    /**
     * 登录
     * 多个参数需要封装为一个Map
     */
    public EmpEntity login(Map<String, Object> param);

    /**
     * 修改用户信息
     */
    public int modEmp(EmpEntity emp);

    /**
     * 修改密码
     * @param params
     * @return
     */
    public int modPwd(Map<String, Object> params);
}
